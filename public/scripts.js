document.addEventListener("DOMContentLoaded", () => {
    const audio = new Audio('/audio/pp.mp3');
    const magic = Math.random() * 10;
    const video = document.getElementById('video-wrapper');
    const brandImage = document.getElementById('brand-image');
    const dialog = document.getElementById('regular-johnsons');

    dialog.showModal();

    dialog.addEventListener("close", (e) => {
        video.classList.remove('hidden');
        brandImage.classList.remove('hidden');
        
        audio.addEventListener('canplay', e => {
            playPP();
        });

        function playPP() {
            if (Math.random() === magic) {
                audio.play();
            }
            setTimeout(playPP, Math.random() * 5000);
        }
    });
});
